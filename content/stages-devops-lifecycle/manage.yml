---
  title: Manage
  description: Learn how GitLab helps gain visibility and insight into how your business is performing. View more here!
  components:
    - name: sdl-cta
      data:
        title: Manage
        aos_animation: fade-down
        aos_duration: 500
        subtitle: Gain visibility and insight into how your business is performing.
        text: |
          GitLab helps teams manage and optimize their software delivery lifecycle with metrics and value stream insight in order to streamline and increase their delivery velocity. Learn more about how GitLab helps to manage your end to end [value stream](/vsm){data-ga-name="value stream" data-ga-location="body"}.
        image:
          url: /nuxt-images/icons/devops/manage-colour.svg
          alt: Manage
    - name: featured-media
      data:
        column_size: 6
        header: Product categories
        header_animation: fade-up
        header_animation_duration: 500
        media:
          - title: Subgroups
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Organize your projects and restrict access to controlled resources.
            link:
              href: https://docs.gitlab.com/ee/user/group/subgroups/
              text: Learn More
              data_ga_name: subgroups learn more
              data_ga_location: body
          - title: Audit Events
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Track important events for review and compliance such as who performed certain actions and the time they happened.
            link:
              href: https://docs.gitlab.com/ee/administration/audit_events.html
              text: Learn More
              data_ga_name: audit events learn more
              data_ga_location: body
          - title: Audit Reports
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            link:
              href: https://docs.gitlab.com/ee/administration/audit_reports.html
              text: Learn More
              data_ga_name: audit reports learn more
              data_ga_location: body
          - title: Compliance Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Provide customers with the tools and features necessary to manage their compliance programs.
            link:
              href: https://docs.gitlab.com/ee/administration/compliance.html
              text: Learn More
              data_ga_name: compliance management learn more
              data_ga_location: body
          - title: DevOps Reports
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Get an overview of how well your organization is adopting DevOps and to see the impact on your velocity.
            link:
              href: https://docs.gitlab.com/ee/user/admin_area/analytics/dev_ops_report.html
              text: Learn More
              data_ga_name: devops reports learn more
              data_ga_location: body
          - title: Value Stream Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Visualize, manage and optimize the flow of work through the DevOps lifecycle value stream.
            link:
              href: /solutions/value-stream-management/
              text: Learn More
              data_ga_name: value stream management learn more
              data_ga_location: body
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/enablement/){data-ga-name="enablement direction" data-ga-location="body"}.
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Plan
            icon:
              url: /nuxt-images/icons/devops/plan-colour.svg
              alt: Plan
            aos_animation: zoom-in
            aos_duration: 500
            text: |
              Regardless of your process, GitLab provides powerful planning tools to keep everyone synchronized.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/plan/
              data_ga_name: plan
              data_ga_location: body
          - title: Create
            icon:
              url: /nuxt-images/icons/devops/create-colour.svg
              alt: Create
            aos_animation: zoom-in
            aos_duration: 1000
            text: |
              Create, view, and manage code and project data through powerful branching tools.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/create/
              data_ga_name: create
              data_ga_location: body
          - title: Verify
            icon:
              url: /nuxt-images/icons/devops/verify-colour.svg
              alt: Verify
            aos_animation: zoom-in
            aos_duration: 1500
            text: |
              Keep strict quality standards for production code with automatic testing and reporting.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/verify/
              data_ga_name: verify
              data_ga_location: body
