---
  title: Industry Analyst Research | GitLab
  description: What top Industry Analysts are saying about GitLab
  components:
    - name: 'call-to-action'
      data:
        title: What top Industry Analysts are saying about GitLab
        subtitle: Discover what third-party industry analysts are saying about GitLab so that through their in-depth research on how GitLab solves customer challenges, you can learn how the one DevOps platform sets you up for success in the evolving technology landscape.
        centered_by_default: true
        column_size: 8
    - name: 'tabs-menu'
      data:
        column_size: 12
        menus:
          - id_tag: featured-reports
            text: Featured Reports
            data_ga_name: Featured Reports
            data_ga_location: header
          - id_tag: analyst-report-categories
            text: Analyst Report Categories
            data_ga_name: Analyst Report Categories
            data_ga_location: header
          - id_tag: mentions-reports
            text: Gitlab Mentions
            data_ga_name: Gitlab Mentions
            data_ga_location: header
            href: '/analysts#mentions-reports'
          - id_tag: ar-contacts
            text: AR Contacts
            data_ga_name: AR Contacts
            data_ga_location: header
    - name: 'featured-reports'
      data:
        title: Featured Reports
        metadata:
          id_tag: featured-reports
        reports:
          - name: GitLab Takes DevOps Platform to an IPO
            description: IDC Market Perspective
            logo:  /nuxt-images/logos/idc-logo.svg
            img_url: /nuxt-images/analysts/idc-report.png
            img_alt: idc report
            cta_url: https://learn.gitlab.com/idc-gitlab-ipo/idc_gitlab_devops
          - name: 2021 Gartner® Market Guide for Value Stream Delivery Platforms
            description: GitLab Cited as Representative Vendor in 2021 Market Guide
            logo: /nuxt-images/logos/gartner-logo.svg
            img_url: /nuxt-images/analysts/gartner-report.png
            img_alt: gartner report
            cta_url: /analysts/gartner-vsdp21/
          - name: 2021 Gartner® Magic Quadrant for Enterprise Agile Planning Tools
            description: GitLab Named a Leader
            logo: /nuxt-images/logos/gartner-logo.svg
            img_url: /nuxt-images/analysts/gartner-quadrant.png
            img_alt: gartner quadrant
            cta_url: /analysts/gartner-eapt21/
          - name: 2021 Gartner® Magic Quadrant for Application Security Testing
            description: GitLab Named a Challenger
            logo: /nuxt-images/logos/gartner-logo.svg
            img_url: /nuxt-images/analysts/gartner-security-quadrant.png
            img_alt: gartner security quadrant
            cta_url: /analysts/gartner-ast21/
          - name: 'The 2019 Forrester Wave™: Cloud-Native Continuous Integration Tools'
            description: Forrester names GitLab a leader for Continuous Integration in their 2019 evaluation
            logo: /nuxt-images/logos/forrester-logo.svg
            img_url: /nuxt-images/analysts/forrester-report.png
            img_alt: forrester report
            cta_url: /analysts/forrester-cloudci19/
    - name: 'copy-faq'
      data:
        title: Categories of analyst reports relevant to GitLab
        metadata:
          id_tag: analyst-report-categories
        description: 'As a single application for the entire DevOps lifecycle, GitLab is relevant across multiple analyst categories and reports. We believe GitLab is relevant in the following analyst categories:'
        hide_tanuki: true
        faq:
          groups:
            - header: Report Categories Relevant to GitLab
              questions:
                - answer: >-
                    + API Security Testing

                    + Application Performance Monitoring (APM)

                    + Application Security Testing (AST) / Static Application Security Testing

                    + Browser-based IDEs

                    + Container Management / Public Cloud Container Services

                    + Design Management

                    + DesignOps

                    + DevOps

                    + DevSecOps

                    + Enterprise Agile Planning Tools (EAPT)

                    + GitOps / Infrastructure as Code (IAC)

                    + Hypothesis-Driven Development

                    + Incident Response / Incident Managment

                    + Integrated Software Development Platforms

                    + IT Infrastructure Monitoring

                    + MLOps / ModelOps

                    + Observability

                    + Performance Engineering

                    + Progressive Delivery

                    + Software Composition Analysis (SCA)

                    + Software Supply Chain Security

                    + Value Stream Delivery Platforms (VSDP)

                    + Value Stream Management (VSM)

                    + Value Stream Management Platforms (VSMP)

                    + Zero Trust Security



            - header: Additional Categories of Interest
              questions:
                - answer: >-
                    * AIOps

                    * Application Portfolio Management

                    * Continuous Autonomous Validation and Verification

                    * Digital Experience Management (DEM)

                    * Innersourcing

                    * Low-Code Application Platforms / Low-code Development Platforms / Low-Code Development Technologies

                    * IT Service Management (ITSM)

                    * Multiexperience Development Platforms (MXDP)

                    * Project & Portfolio Management (PPM) / Strategic Portfolio Management

                    * Service Orchestration and Automation Platforms (SOAP)

                    * Security Orchestration, Automation and Response (SOAR)
    - name: 'copy-faq'
      data:
        title: GitLab Mentions
        metadata:
          id_tag: mentions-reports
        description: |
          Here are links to reports where GitLab is mentioned within the context of the report. GitLab usually does not have rights to distribute these reports,
          which means you will likely have access to them only if you have a subscription to that analyst's reports through your own organization.
          Clicking these links will bring you to the report on that analyst company's web site, and you will need to log in to download or view the report.
        faq:
          groups:
            - header: Forrester reports mentioning GitLab
              questions:
                - answer: >-
                    [Streamline Process Management With Lean And Agile Thinking, February 8, 2022](https://www.forrester.com/report/streamline-process-management-with-lean-and-agile-thinking/RES157444){.answer__link}

                    [Charter A Corporate Cloud Platform Team, January 20, 2022](https://www.forrester.com/report/charter-a-corporate-cloud-platform-team/RES176990){.answer__link}

                    [Build Better Teams By Playing Video Games At Work, December 30, 2021](https://www.forrester.com/report/build-better-teams-by-playing-video-games-at-work/RES176915){.answer__link}

                    [How To Make Hybrid Work, November 15, 2021](https://www.forrester.com/report/how-to-make-hybrid-work/RES176551){.answer__link}

                    [The Forrester Guide To Hybrid Cloud, September 16, 2021](https://www.forrester.com/report/the-forrester-guide-to-hybrid-cloud/RES143160){.answer__link}

                    [The Forrester Wave™: Software Composition Analysis, Q3 2021, August 18, 2021](https://www.forrester.com/report/the-forrester-wave-tm-software-composition-analysis-q3-2021/RES176091){.answer__link}

                    [Modern Development Metrics That Really Matter, August 2, 2021](https://www.forrester.com/report/modern-development-metrics-that-really-matter/RES175684){.answer__link}

                    [Now Tech: Software Composition Analysis, Q2 2021, April 7, 2021](https://www.forrester.com/report/now-tech-software-composition-analysis-q2-2021/RES161774){.answer__link}

                    [Seize The Anywhere-Work Opportunity By Taking Calculated Risks, March 29, 2021](https://www.forrester.com/report/seize-the-anywhere-work-opportunity-by-taking-calculated-risks/RES165212){.answer__link}

                    [China Tech Market Outlook, 2021 To 2022, January 14, 2021](https://www.forrester.com/report/china-tech-market-outlook-2021-to-2022/RES158362){.answer__link}

                    [The Forrester Wave™: Static Application Security Testing, Q1 2021, January 11, 2021](https://www.forrester.com/report/the-forrester-wave-static-application-security-testing-q1-2021/RES162015){.answer__link}

                    [Harness ChatOps To Empower Remote Collaboration, December 21, 2020](https://www.forrester.com/report/harness-chatops-to-empower-remote-collaboration/RES153381){.answer__link}

                    [COVID Drives M&A Activity In DevOps And IT Management, December 4, 2020](https://www.forrester.com/blogs/covid-drives-ma-activity-in-devops-and-it-management/){.answer__link}

                    [Use The Lessons Of 2020 To Create Your Anywhere-Work Strategy, November 7, 2020](https://www.forrester.com/report/use-the-lessons-of-2020-to-create-your-anywhere-work-strategy/RES160357){.answer__link}

                    [Europe’s Digital Renaissance Starts Now, November 11, 2020](https://www.forrester.com/report/europes-digital-renaissance-starts-now/RES162001){.answer__link}

                    [API Insecurity: The Lurking Threat In Your Software, October 22, 2020](https://www.forrester.com/report/api-insecurity-the-lurking-threat-in-your-software/RES142080){.answer__link}

                    [The Forrester Tech Tide™: Application Security, Q4 2020, October 8, 2020](https://www.forrester.com/report/the-forrester-tech-tide-application-security-q4-2020/RES154237){.answer__link}

                    [The Business Of Belonging, September 16, 2020](https://www.forrester.com/report/the-business-of-belonging/RES161444){.answer__link}

                    [The Forrester Guide To Configuration Management, September 11, 2020](https://www.forrester.com/report/the-forrester-guide-to-configuration-management/RES139754?ref_search=3408741_1648085294081){.answer__link}

                    [Rethink Your CMDB, September 11, 2020](https://www.forrester.com/report/rethink-your-cmdb/RES139753){.answer__link}

                    [Build Your Operations Organization With Product Team Principles, September 9, 2020](https://www.forrester.com/report/build-your-operations-organization-with-product-team-principles/RES156920){.answer__link}

                    [Now Tech: Static Application Security Testing, Q3 2020, August 6, 2020](https://www.forrester.com/report/Now-Tech-Static-Application-Security-Testing-Q3-2020/RES161475?objectid=RES161475){.answer__link}

                    [The Forrester Wave™: Value Stream Management Solutions, Q3 2020, July 15, 2020 ](https://www.forrester.com/report/The-Forrester-Wave-Value-Stream-Management-Solutions-Q3-2020/RES159825){.answer__link}

                    [The State Of Remote Work, 2020, July 6, 2020](https://www.forrester.com/report/The-State-Of-Remote-Work-2020/RES139899){.answer__link}

                    [Now Tech: Zero Trust Solution Providers, Q2 2020, Updated May 27, 2020](https://www.forrester.com/report/Now-Tech-Zero-Trust-Solution-Providers-Q2-2020/RES157552){.answer__link}

                    [Now Tech: Value Stream Management Tools, Q2 2020, April 29, 2020](https://www.forrester.com/report/Now-Tech-Value-Stream-Management-Tools-Q2-2020/RES159376){.answer__link}

                    [Now Tech: Continuous Delivery And Release Automation, Q2 2020, April 9, 2020](https://www.forrester.com/report/Now-Tech-Continuous-Delivery-And-Release-Automation-Q2-2020/RES158884){.answer__link}

                    [It's Go Time For Application And Infrastructure Dependency Mapping (AIDM), February 7, 2020](https://www.forrester.com/report/Its-Go-Time-For-Application-And-Infrastructure-Dependency-Mapping-AIDM/RES141653){.answer__link}
            - header: Gartner reports mentioning GitLab
              questions:
                - answer: >-
                    [Managing Machine Identities, Secrets, Keys and Certificates, March 16, 2022](https://www.gartner.com/document/4012570){.answer__link}

                    [Technology Insight for Digital Product Design Platforms, March 7, 2022](https://www.gartner.com/document/3993492){.answer__link}

                    [Assessing Red Hat OpenShift Container Platform for Cloud-Native Application Delivery on Kubernetes, February 28, 2022](https://www.gartner.com/document/4011931){.answer__link}

                    [Quick Answer: How Should We Manage Our Atlassian Contract?, February 23, 2022](https://www.gartner.com/document/3999736?ref=solrResearch&refval=320309707){.answer__link}

                    [Quick Answer: How to Create a Frictionless Onboarding Experience for Software Engineers, February 11, 2022](https://www.gartner.com/document/3997026){.answer__link}

                    [Innovation Insight for Continuous Compliance Automation, February 11, 2022](https://www.gartner.com/document/3988611){.answer__link}

                    [Gartner Peer Connect Perspectives: What Are Product Owners’ Roles and Responsibilities and Whom Should They Report To?, January 19, 2022](https://www.gartner.com/document/4010508){.answer__link}

                    [How to Deploy and Perform Application Security Testing, December 21, 2021](https://www.gartner.com/document/3982363){.answer__link}

                    [Product Owner Essentials, December 14, 2021](https://www.gartner.com/document/4009472){.answer__link}

                    [Predicts 2022: Modernizing Software Development is Key to Digital Transformation, December 3, 2021](https://www.gartner.com/document/4009060){.answer__link}

                    [Emerging Technology Horizon for Enterprise Software, 2021, November 30, 2021](https://www.gartner.com/document/4008859){.answer__link}

                    [Drive Innovation by Enabling Innersource, November 29, 2021](https://www.gartner.com/document/4008818){.answer__link}

                    [Market Guide for Value Stream Management Platforms, November 10, 2021](https://www.gartner.com/document/4008133){.answer__link}

                    [Solution Comparison for Low-Code Application Platforms, October 18, 2021](https://www.gartner.com/document/4007038){.answer__link}

                    [Magic Quadrant for Industrial IoT Platforms, October 18, 2021](https://www.gartner.com/document/4006918?toggle=1&refval=320311944){.answer__link}

                    [2022 Planning Guide for IT Operations and Cloud Management, October 11, 2021](https://www.gartner.com/document/4006613){.answer__link}

                    [Emerging Technologies and Trends Impact Radar: Enterprise Software, October 4, 2021](https://www.gartner.com/document/4006476?ref=solrResearch&refval=320367466){.answer__link}

                    [Quick Answer: How Should We Test Enterprise SaaS Applications?, September 29, 2021](https://www.gartner.com/document/4006309){.answer__link}

                    [Magic Quadrant for Full Life Cycle API Management, September 28, 2021](https://www.gartner.com/document/4006268?toggle=1&refval=320367466&ref=solrResearch){.answer__link}

                    [Using Emerging Service Connectivity Technology to Optimize Microservice Application Networking, September 21, 2021](https://www.gartner.com/document/4006008?ref=solrResearch&refval=320367466){.answer__link}

                    [Critical Capabilities for Enterprise Low-Code Application Platforms, September 21, 2021](https://www.gartner.com/document/4005973?ref=solrResearch&refval=320367466){.answer__link}

                    [Market Guide for Software Composition Analysis, September 14, 2021](https://www.gartner.com/document/4005759){.answer__link}

                    [Gartner Peer Insights ‘Voice of the Customer’: Enterprise Agile Planning Tools, September 9, 2021](https://www.gartner.com/document/4005598?ref=solrResearch&refval=320369208){.answer__link}

                    [Innovation Insight for Cloud-Native Application Protection Platforms, August 25, 2021](https://www.gartner.com/document/4005115){.answer__link}

                    [The Innovation Leader’s Guide to Navigating the Cloud-Native Container Ecosystem, August 25, 2021](https://www.gartner.com/document/4004870){.answer__link}

                    [Hype Cycle for Enterprise Architecture, 2021, August 6, 2021](https://www.gartner.com/document/4004444){.answer__link}

                    [Hype Cycle for Software Engineering, 2021, July 27, 2021](https://www.gartner.com/document/4004063){.answer__link}

                    [Hype Cycle for Open-Source Software, 2021, July 26, 2021](https://www.gartner.com/document/4004007){.answer__link}

                    [Hype Cycle for ITSM, 2021, July 21, 2021](https://www.gartner.com/document/4003887){.answer__link}

                    [Hype Cycle for I&O Automation, 2021, July 16, 2021](https://www.gartner.com/document/4003665){.answer__link}

                    [How Software Engineering Leaders Can Mitigate Software Supply Chain Security Risks, July 15, 2021](https://www.gartner.com/document/4003625){.answer__link}

                    [Hype Cycle for Communications Service Provider Operations, 2021, July 14, 2021](https://www.gartner.com/document/4003571){.answer__link}

                    [Hype Cycle for ICT in China, 2021, July 13, 2021](https://www.gartner.com/document/4003537){.answer__link}

                    [Hype Cycle for Agile and DevOps, 2021, July 12, 2021](https://www.gartner.com/document/4003540){.answer__link}

                    [Hype Cycle for Application Security, 2021, July 12, 2021](https://www.gartner.com/document/4003469){.answer__link}

                    [Market Share Analysis: Application Development Software, Worldwide, 2020, July 7, 2021](https://www.gartner.com/document/4003344){.answer__link}

                    [Solution Path for Continuous Delivery With DevOps, June 16, 2021](https://www.gartner.com/document/4002645){.answer__link}

                    [2 Important Steps to Drive Action From Your Procurement Policy, June 4, 2021](https://www.gartner.com/document/4002276){.answer__link}

                    [The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams, June 4, 2021](https://www.gartner.com/document/3979558){.answer__link}

                    [Magic Quadrant for Application Security Testing, May 27, 2021](https://www.gartner.com/document/4001946){.answer__link}

                    [Solution Criteria for AIOps Platforms, May 27, 2021](https://www.gartner.com/document/4002020){.answer__link}

                    [Critical Capabilities for Application Security Testing, May 26, 2021](https://www.gartner.com/document/4001984){.answer__link}

                    [Vendor Rating: Amazon, May 17, 2021](https://www.gartner.com/document/4001655){.answer__link}

                    [Cool Vendors in Cloud Computing, May 13, 2021](https://www.gartner.com/document/4001590){.answer__link}

                    [How Software Engineering Leaders Can Use Value Stream Metrics to Improve Agile Effectiveness, May 4, 2021](https://www.gartner.com/document/4001266){.answer__link}

                    [Ignition Guide to Updating Your Procurement Policy, May 3, 2021](https://www.gartner.com/document/4001227){.answer__link}

                    [Cool Vendors for Software Engineering Technologies, May 3, 2021](https://www.gartner.com/document/4001232){.answer__link}

                    [Innovation Insight for Continuous Infrastructure Automation, April 27, 2021](https://www.gartner.com/document/4001005){.answer__link}

                    [Critical Capabilities for Enterprise Agile Planning Tools, April 21, 2021](https://www.gartner.com/document/4000790){.answer__link}

                    [Magic Quadrant for Enterprise Agile Planning Tools, April 20, 2021](https://www.gartner.com/document/4000687){.answer__link}

                    [12 Things to Get Right for Successful DevSecOps, April 9, 2021](https://www.gartner.com/document/3978490){.answer__link}

                    [Tool: Open-Source Software Governance Policy Template, March 16, 2021](https://www.gartner.com/document/3999408){.answer__link}

                    [Assessing Site Reliability Engineering (SRE) Principles for Building a Reliability-Focused Culture, January 28, 2021](https://www.gartner.com/document/3996091){.answer__link}

                    [Solution Criteria for Hyperconverged Infrastructure Software, January 27, 2021](https://www.gartner.com/document/3995982){.answer__link}

                    [To Automate Your Automation, Apply Agile and DevOps Practices to Infrastructure and Operations, January 6, 2021](https://www.gartner.com/document/3995225){.answer__link}

                    [How to Automate Server Provisioning and Configuration Management, December 23, 2020](https://www.gartner.com/document/3994873){.answer__link}

                    [Emerging Technology Horizon for Enterprise Software, December 17, 2020](https://www.gartner.com/document/3994623){.answer__link}

                    [Four Steps to Adopt Open-Source Software as Part of Your Test Automation Stack, December 16, 2020](https://www.gartner.com/document/3947142){.answer__link}

                    [Magic Quadrant for Enterprise Architecture Tools, December 14, 2020](https://www.gartner.com/document/3994442){.answer__link}

                    [Implementing an Enterprise Open-Source Machine Learning Stack, December 3, 2020](https://www.gartner.com/document/3993900){.answer__link}

                    [How to Choose Your Best-Fit Vendor for Test Management, December 2, 2020](https://www.gartner.com/document/3942118){.answer__link}

                    [Ensure Safe and Successful Usage of Open-Source Software With a Comprehensive Governance Policy, November 20, 2020](https://www.gartner.com/document/3993432){.answer__link}

                    [Tech CEOs Can Expand the Container-Based Infrastructure Market by Supporting Edge Computing and Machine Learning, November 19, 2020](https://www.gartner.com/document/3993386){.answer__link}

                    [Market Trends: The Rise of Cloud-Native Technology Ecosystems (Container Perspective), November 2, 2020](https://www.gartner.com/document/3992596){.answer__link}

                    [Emerging Technologies and Trends Impact Radar: Enterprise Software, October 16, 2020](https://www.gartner.com/document/3991896){.answer__link}

                    [Gartner Peer Insights ‘Voice of the Customer’: Enterprise Agile Planning Tools, October 13, 2020](https://www.gartner.com/document/3991652){.answer__link}

                    [Gartner Peer Insights ‘Voice of the Customer’: Application Security Testing, October 9, 2020](https://www.gartner.com/document/3991601){.answer__link}

                    [2021 Planning Guide for IT Operations and Cloud Management, October 9, 2020](https://www.gartner.com/document/3991595){.answer__link}

                    [Market Guide for DevOps Value Stream Delivery Platforms, 28 September, 2020](https://www.gartner.com/document/3991050){.answer__link}

                    [Four Steps to Adopt Open-Source Software as Part of the DevOps Toolchain, updated 19 August, 2020](https://www.gartner.com/document/3900266?ref=solrResearch&refval=260083185){.answer__link}

                    [Competitive Landscape: Custom Software Development Services, 19 August, 2020](https://www.gartner.com/document/3989290?ref=solrResearch&refval=260083864){.answer__link}

                    [Market Guide for Software Composition Analysis, 18 August, 2020](https://www.gartner.com/document/3989235?ref=solrResearch&refval=260084245){.answer__link}

                    [Analyze Value Stream Metrics to Optimize DevOps Delivery, 7 August, 2020 ](https://www.gartner.com/document/3988532?ref=solrResearch&refval=260084582){.answer__link}

                    [Hype Cycle for I&O Automation, 2020, 4 August, 2020](https://www.gartner.com/document/3988394?ref=solrResearch&refval=260084814){.answer__link}

                    [Hype Cycle for Application Architecture and Development, 2020, 31 July, 2020](https://www.gartner.com/document/3988288?ref=solrResearch&refval=260085065){.answer__link}

                    [Emerging Technologies: Functionality Spectrum for Cloud Workload Protection Platforms, 2020, 30 July, 2020](https://www.gartner.com/document/3988277?ref=solrResearch&refval=260085350){.answer__link}

                    [CTO’s Guide to Containers and Kubernetes — Answering the Top 10 FAQs, 27 July, 2020](https://www.gartner.com/document/3988026?ref=solrResearch&refval=2600855725){.answer__link}

                    [Hype Cycle for ITSM, 2020, 27 July, 2020](https://www.gartner.com/document/3988035?ref=solrResearch&refval=260085949){.answer__link}

                    [Hype Cycle for Application Security, 2020, 27 July, 2020](https://www.gartner.com/document/3988043?ref=solrResearch&refval=260086210){.answer__link}

                    [Hype Cycle for Agile and DevOps, 2020, 15 July, 2020](https://www.gartner.com/document/3987588?ref=solrResearch&refval=257273540){.answer__link}

                    [Hype Cycle for Open-Source Software, 2020, 10 July, 2020](https://www.gartner.com/document/3987447?ref=solrResearch&refval=257273720){.answer__link}

                    [Vendor Rating: Amazon, 7 July, 2020](https://www.gartner.com/document/3987230?ref=solrResearch&refval=257273861){.answer__link}

                    [How to Build an Effective Remote Testing Competency, 30 June, 2020](https://www.gartner.com/document/3986969?ref=solrResearch&refval=257273955){.answer__link}

                    [Structuring Application Security Tools and Practices for DevOps and DevSecOps, 18 June, 2020](https://www.gartner.com/document/3986517?ref=solrResearch&refval=254019907){.answer__link}

                    [7 Tips to Set Up an Application Security Program Without Breaking the Bank, 11 June, 2020](https://www.gartner.com/document/3986206?ref=solrResearch&refval=254020933){.answer__link}

                    [Guidance Framework for Choosing What to Automate to Increase Application Delivery Agility, 4 June, 2020](https://www.gartner.com/document/3985951?ref=solrResearch&refval=254026239){.answer__link}

                    [Guidance Framework for Deploying Centralized Log Monitoring, 26 May, 2020](https://www.gartner.com/document/3985617?ref=solrResearch&refval=254022190){.answer__link}

                    [Solution Path for Evolving to Next-Generation Enterprise Networks, 21 May, 2020](https://www.gartner.com/document/3985473?ref=solrResearch&refval=254023330){.answer__link}

                    [Cool Vendors in Agile and DevOps, 20 May, 2020](https://www.gartner.com/document/3985396?ref=solrAll&refval=252791701){.answer__link}

                    [3 Steps to Sustain Productivity and Collaboration in Remote Agile and DevOps Teams, 6 May, 2020](https://www.gartner.com/document/3984714?ref=solrAll&refval=252794902){.answer__link}

                    [Magic Quadrant for Application Security Testing, 29 April, 2020](/resources/report-gartner-mq-ast/){.answer__link}

                    [Critical Capabilities for Enterprise Agile Planning Tools, 29 April, 2020](https://www.gartner.com/document/3984344?ref=solrAll&refval=252794289/){.answer__link}

                    [Critical Capabilities for Application Security Testing, 27 April, 2020](https://www.gartner.com/document/3984230?ref=solrAll&refval=252794165){.answer__link}

                    [Magic Quadrant for Enterprise Agile Planning Tools, 21 April, 2020](https://www.gartner.com/document/3983813?toggle=1&refval=252793559&ref=solrAll){.answer__link}

                    [Best Practices to Enable Continuous Delivery With Containers and DevOps, 16 April, 2020](https://www.gartner.com/document/3983596?ref=solrAll&refval=252793361){.answer__link}

                    [Setting Up Remote Agile in a Hurry?, 2 April, 2020 ](https://www.gartner.com/document/3982950?ref=solrResearch&refval=246150423){.answer__link}

                    [How Application Leaders Can Address the Unprecedented Organizational and Cultural Impacts of COVID-19, 31 March, 2020](https://www.gartner.com/document/3982819?ref=solrResearch&refval=245453908){.answer__link}

                    [How to Deploy and Perform Application Security Testing, 20 March, 2020](https://www.gartner.com/document/3982363?ref=solrResearch&refval=245455270){.answer__link}

                    [Gartner Peer Insights ‘Voice of the Customer’: Application Release Orchestration, 12 March, 2020](https://www.gartner.com/document/3982008?ref=solrResearch&refval=245454554){.answer__link}

                    [Assessing HashiCorp Terraform for Provisioning Cloud Infrastructure, 6 March, 2020](https://www.gartner.com/document/3981898?ref=TypeAheadSearch){.answer__link}

                    [Essential Skills for Automation Engineers, 26 February, 2020](https://www.gartner.com/document/3981475?ref=TypeAheadSearch){.answer__link}

                    [How to Automate Your Network Using DevOps Practices and Infrastructure as Code, 15 January, 2020](https://www.gartner.com/document/3979611){.answer__link}

                    [The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams, 14 January, 2020](https://www.gartner.com/document/3979558?ref=solrResearch&refval=239809121){.answer__link}
            - header: IDC reports mentioning GitLab
              questions:
                - answer: >-
                    [The Impact of the Russia-Ukraine War on the Global ICT Market Landscape — IDC's First Take, March 4, 2022](https://www.idc.com/getdoc.jsp?containerId=EUR148926122&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC Market Glance: DevSecOps, 1Q22, February, 2022](https://www.idc.com/getdoc.jsp?containerId=US48599722&pageType=PRINTFRIENDLY){.answer__link}

                    [Worldwide Development Languages, Environments, and Tools Market Shares, 2020: Growth Increases as Enterprises Accelerate Digital Innovation Due to COVID-19, December, 2021](https://www.idc.com/getdoc.jsp?containerId=US47924322&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC's Worldwide Enterprise Infrastructure Workloads Taxonomy, 2H21, November, 2021](https://www.idc.com/getdoc.jsp?containerId=US48325721&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC's Worldwide Semiannual Enterprise Infrastructure Tracker: Workloads Taxonomy, 2021, November, 2021](https://www.idc.com/getdoc.jsp?containerId=US48332521&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC Market Glance: Mainframe DevOps, 4Q21, November, 2021](https://www.idc.com/getdoc.jsp?containerId=US47150021&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC FutureScape: Worldwide Developer and DevOps 2022 Predictions, October, 2021](https://www.idc.com/getdoc.jsp?containerId=US47148521&pageType=PRINTFRIENDLY){.answer__link}

                    [Worldwide Software Change, Configuration, and Process Management Forecast, 2021–2025: Agile, Digital Innovation, and DevOps Impel Growth Expectations, September, 2021](https://www.idc.com/getdoc.jsp?containerId=US46416621&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC Market Glance: DevOps, 3Q21, August, 2021](https://www.idc.com/getdoc.jsp?containerId=US48129721&pageType=PRINTFRIENDLY){.answer__link}

                    [Worldwide DevOps Software Tools Market Shares, 2020: Growth Fueled by Accelerated Digital Transformation, July, 2021](https://www.idc.com/getdoc.jsp?containerId=US48050921&pageType=PRINTFRIENDLY){.answer__link}

                    [Worldwide DevSecOps Software Tools Market Shares, 2020: Strong Growth as DevOps Teams Prioritize Security, July, 2021](https://www.idc.com/getdoc.jsp?containerId=US48051321&pageType=PRINTFRIENDLY){.answer__link}

                    [Worldwide Automated Software Quality Market Shares, 2020: Rapid Digitalization Impels Ongoing Quality Adoption, June, 2021](https://www.idc.com/getdoc.jsp?containerId=US47916021&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC TechBrief: GitOps, June, 2021](https://www.idc.com/getdoc.jsp?containerId=US47775921&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC's Worldwide Software Taxonomy, 2021, April, 2021](https://www.idc.com/getdoc.jsp?containerId=US47588620&pageType=PRINTFRIENDLY){.answer__link}

                    [Five Open Source Projects to Watch Carefully in 2021, March, 2021](https://www.idc.com/getdoc.jsp?containerId=US47527321&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC's Worldwide Intelligent CloudOps Software Taxonomy, 2021, February, 2021](https://www.idc.com/getdoc.jsp?containerId=US47033721&pageType=PRINTFRIENDLY){.answer__link}

                    [IBM Partners with GitLab on DevOps Automation Platform, January, 2021](https://www.idc.com/getdoc.jsp?containerId=lcUS47419621&pageType=PRINTFRIENDLY){.answer__link}

                    [Leveraging DevOps Delivery for Rapid Responsiveness and Digital Innovation Moving into the "New Normal", December, 2020](https://www.idc.com/getdoc.jsp?containerId=US47148421&pageType=PRINTFRIENDLY){.answer__link}

                    [Worldwide Software Change, Configuration, and Process Management Forecast Update, 2020–2024: Adaptive Software Demand Drives SCCPM Forecast Growth, December, 2020](http://idc.com/getdoc.jsp?containerId=US47091620&pageType=PRINTFRIENDLY){.answer__link}

                    [Understanding the Role and Impact of Open Core Open Source Software Development, December, 2020](https://www.idc.com/getdoc.jsp?containerId=US47093320&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC Market Glance: Application Development, 4Q20, December, 2020](https://www.idc.com/getdoc.jsp?containerId=US46417621&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC FutureScape: Worldwide Developer and DevOps 2021 Predictions, October, 2020](https://www.idc.com/getdoc.jsp?containerId=US46417220&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC FutureScape: Worldwide Future of Digital Innovation 2021 Predictions, October, 2020](https://www.idc.com/getdoc.jsp?containerId=US46417320&pageType=PRINTFRIENDLY){.answer__link}

                    [GitLab — The Incremental Building of a DevOps Platform, August, 2020](https://www.idc.com/getdoc.jsp?containerId=US46770820){.answer__link}

                    [Worldwide DevOps Software Tools Market Shares, 2019: Leaders Drive Double-Digit Growth, July, 2020](https://www.idc.com/getdoc.jsp?containerId=US45188620){.answer__link}

                    [IDC Market Glance: Collaborative Innovation, 2Q20, June, 2020](https://www.idc.com/getdoc.jsp?containerId=US46513219){.answer__link}

                    [IDC Market Glance: Service Mesh Ecosystem, 1H20, June, 2020](https://www.idc.com/getdoc.jsp?containerId=US46447320){.answer__link}

                    [IBM Think 2020 — Pervasive AI Accelerates Business Transformation, June, 2020](https://www.idc.com/getdoc.jsp?containerId=US46453720){.answer__link}

                    [IDC Market Glance: DevOps Application Life-Cycle Management, 1Q20, March, 2020](https://www.idc.com/getdoc.jsp?containerId=US46176120){.answer__link}

                    [JFrog Announces a New Unified DevOps Platform, February, 2020](https://www.idc.com/getdoc.jsp?containerId=lcUS45187820&pageType=PRINTFRIENDLY){.answer__link}

                    [IDC MarketScape: Worldwide Agile Project and Portfolio Management 2020 Vendor Assessment — Enabling Business Velocity for Digital Innovation, January 2020](https://www.idc.com/getdoc.jsp?containerId=US44483219){.answer__link}

                    [IDC Market Glance Update: Future of Work, 1Q20, January, 2020](https://www.idc.com/getdoc.jsp?containerId=US45398219){.answer__link}
    - name: 'contacts'
      data:
        title: Analyst Relations Contacts
        metadata:
          id_tag: ar-contacts
        description: |
            Please reach out to GitLab Analyst Relations for updates on GitLab’s current capabilities and go-to-market, positioning, and product strategies, as well as requests for participation in signature research.
        column_size: 6
        contacts:
          - name: Ryan Ragozzine
            job: Analyst Relations Manager
            email: rragozzine@gitlab.com
            phone: 1-619-871-7497
          - name: Laura Clymer
            job: Director of Market Strategy & Insights
            email: lclymer@gitlab.com
            phone: 1-512-578-9542


