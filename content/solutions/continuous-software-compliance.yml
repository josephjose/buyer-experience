---
  title: Continuous Software Compliance
  description: Build in compliance for less risk and greater efficiency
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: Continuous Software Compliance
        subtitle: Build in compliance for less risk and greater efficiency
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Start your free trial
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Questions? Contact us
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg
          alt: "Image: gitLab for continuous software compliance"
    - name: 'by-solution-intro'
      data:
        text:
          highlight: Software compliance is no longer just about checking boxes.
          description: Cloud native applications present entirely new attack surfaces via containers, orchestrators, web APIs, and other infrastructure-as-code. These new attack surfaces, along with complex DevOps toolchains, have resulted in notorious software supply chain attacks and led to new regulatory requirements. Continuous software compliance is becoming a critical way to manage risk inherent in cloud native applications and DevOps automation - beyond merely reducing security flaws within the code itself.
    - name: 'by-solution-benefits'
      data:
        title: Compliance. Security. Simplified.
        image:
          image_url: "/nuxt-images/solutions/benefits/security.jpeg"
          alt: GitLab for Public Sector
        is_accordion: true
        items:
          - header: Confidence with every commit
            icon: /nuxt-images/icons/shield-check.svg
            text: License compliance and security scans automatically happen with every committed code change.
          - header: Compliance pipelines for ensuring enforcement 
            icon: /nuxt-images/icons/pipeline.svg
            text: Ensure that important policies are enforced — whether it’s standard regulatory controls or your own policy framework.
          - header: Simplify audits
            icon: /nuxt-images/icons/magnifying-glass-code.svg
            text: Automated policies simplify audits with visibility and traceability.
          - header: All in one place
            icon: /nuxt-images/icons/devsecops-loop.svg
            text: With GitLab, you have one application to manage. No silos, and nothing lost in translation.
    - name: 'by-solution-value-prop'
      data:
        title: One DevOps platform for compliance
        cards:
          - title: Built-in controls
            description: Software compliance can be difficult when it is disconnected from the software development process. Organizations need a compliance program that is built-in, not bolted-on, to their existing workflows and processes. Learn more by downloading the <a href="https://page.gitlab.com/resources-ebook-modernsoftwarefactory.html" data-ga-name="Guide to Software Supply Chain Security" data-ga-location="body">Guide to Software Supply Chain Security</a>
            icon: "/nuxt-images/icons/visibility.svg"
          - title: Policy automation
            description: Compliance guardrails allow rapid software development while reducing risk of non-compliance and of project delays. Auditing is simplified by having everything in one place.
            icon: "/nuxt-images/icons/continuous-integration.svg"
          - title: Shift compliance left
            description: Just as you want to find and fix security flaws early, it’s most efficient to do the same with compliance flaws. Ensuring compliance is integrated into development enables compliance to shift left also
            icon: "/nuxt-images/icons/less-risk.svg"
          - title: Compliance frameworks
            description: Easily apply common compliance settings to a project with a label. 
            list:
              - text: <a href="https://about.gitlab.com/solutions/pci-compliance/" data-ga-name="PCI Compliance" data-ga-location="body" >PCI Compliance</a>
              - text: <a href="https://about.gitlab.com/solutions/hipaa-compliance/" data-ga-name="HIPAA Compliance" data-ga-location="body" >HIPAA Compliance</a>
              - text: <a href="https://about.gitlab.com/solutions/financial-services-regulatory-compliance/" data-ga-name="Financial Services Regulatory Compliance" data-ga-location="body" >Financial Services Regulatory Compliance</a>
              - text: <a href="GDPR" data-ga-name="GDPR" data-ga-location="body" >GDPR</a>
              - text: <a href="https://about.gitlab.com/solutions/iec-62304/" data-ga-name="IEC 62304:2006" data-ga-location="body" >IEC 62304:2006</a>
              - text: <a href="https://about.gitlab.com/solutions/iso-13485/" data-ga-name="ISO 13485:2016" data-ga-location="body" >ISO 13485:2016</a>
              - text: <a href="https://about.gitlab.com/solutions/iso-26262/" data-ga-name="ISO 26262-6:2018" data-ga-location="body" >ISO 26262-6:2018</a>
              - text: Your own policy framework
            icon: "/nuxt-images/icons/web.svg"
    - name: 'by-industry-solutions-block'
      data:
        subtitle: Simplify continuous software compliance
        sub_description: "GitLab's compliance management capabilities aim to create an experience that's simple, friendly, and as frictionless as possible by enabling you to define, enforce and report on compliance policies and frameworks."
        white_bg: true
        sub_image: /nuxt-images/solutions/showcase.png
        solutions:
          - title: Policy Management
            description: Define rules and policies to adhere to compliance frameworks and common controls.
            icon: "/nuxt-images/icons/test-clipboard-checks.svg"
            link_text: Learn More
            link_url: https://docs.gitlab.com/ee/administration/compliance.html
            data_ga_name: common controls
            data_ga_location: solutions block
          - title: Compliant Workflow Automation
            icon: "/nuxt-images/icons/automated-code.svg"
            description:  Compliance automation helps you enforce the defined rules and policies and separation of duties while reducing overall business risk.
            link_text: Learn More
            link_url: https://docs.gitlab.com/ee/administration/compliance.html#compliant-workflow-automation
            data_ga_name: Compliant Workflow Automation
            data_ga_location: solutions block
          - title: Audit Management
            icon: /nuxt-images/icons/magnifying-glass-code.svg
            description: Log activities throughout your DevOps automation to identify incidents and prove adherence to compliance rules and defined policies. Visibility is greater with one platform and no toolchain silos.
            link_text: Learn More
            link_url: https://docs.gitlab.com/ee/administration/compliance.html#audit-management
            data_ga_name: Audit management
            data_ga_location: solutions block
          - title: Security testing and vulnerability management
            description: Ensure security scanning and license compliance for every code change and allow DevOps engineers and security pros alike to track and manage vulnerabilities.
            icon: "/nuxt-images/icons/approve-dismiss.svg"
            link_text: Learn More
            link_url: https://about.gitlab.com/solutions/dev-sec-ops/
            data_ga_name: Security testing and vulnerability management
            data_ga_location: solutions block
          - title: Software Supply Chain Security
            description: Manage the end-to-end attack surfaces of cloud native applications and DevOps automation — beyond traditional application security testing.
            link_text: Learn More
            icon: /nuxt-images/icons/shield-check.svg
            link_url: https://about.gitlab.com/direction/supply-chain/
            data_ga_name: offline environment
            data_ga_location: solutions block
    - name: 'solutions-resource-cards'
      data:
        column_size: 4
        cards:
          - icon: "/nuxt-images/icons/resources/video.svg"
            event_type: "Video"
            header: "Compliant pipelines"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_3.jpg"
            href: "https://www.youtube.com/watch?v=jKA_e_jimoI"
            data_ga_name: "Compliant pipelines"
            data_ga_location: resource cards
          - icon: "/nuxt-images/icons/resources/blog.svg"
            event_type: "Blog"
            header: "Three things you may not know about GitLab security"
            link_text: "Learn more"
            href: "https://about.gitlab.com/blog/2021/11/23/three-things-you-might-not-know-about-gitlab-security/"
            image: "/nuxt-images/resources/resources_1.jpeg"
            data_ga_name: "Three things you may not know about GitLab security"
            data_ga_location: resource cards
          - icon: "/nuxt-images/icons/resources/webcast.svg"
            event_type: "Webinar"
            header: "Secure your software supply chain"
            link_text: "Learn more"
            href: "https://www.youtube.com/watch?v=dZfS4Wt5ZRE"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-gitops.png"
            data_ga_name: "Secure your software supply chain"
            data_ga_location: resource cards
          - icon: "/nuxt-images/icons/resources/ebook.svg"
            event_type: "Ebook"
            header: "Software Supply Chain Security"
            link_text: "Learn more"
            href: "https://learn.gitlab.com/devsecops-aware/software-supply-chain-security-ebook"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-gitlab.png"
            data_ga_name: "Software Supply Chain Security"
            data_ga_location: resource cards
          - icon: "/nuxt-images/icons/resources/case-study.svg"
            event_type: "Case study"
            header: "Chorus case study"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_2.jpeg"
            href: "https://about.gitlab.com/customers/chorus/"
            data_ga_name: "Chorus case study"
            data_ga_location: resource cards
