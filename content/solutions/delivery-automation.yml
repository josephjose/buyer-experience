---
  title: Automated Software Delivery with GitLab
  description: Achieve DevOps Automation - code, build, testing, release automation
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: Automated software delivery
        subtitle: Automation essentials for achieving digital innovation, cloud native transformations and application modernization
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Start your free trial
          url: /free-trial/
        secondary_btn:
          text: Questions? Contact us
          url: /sales/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg
          alt: "Image: gitLab for automated software delivery"
    - name: 'by-solution-intro'
      data:
        text:
          highlight: Automated software delivery with GitLab
          description: will help you adopt cloud native, Kubernetes and multi-cloud with ease, achieve faster velocity with lower failures and improve developer productivity by eliminating repetitive tasks.
    - name: 'by-solution-benefits'
      data:
        title: Why automated software delivery?
        image:
          image_url: "/nuxt-images/solutions/benefits/by-solution-benefits-delivery-automation.jpeg"
          alt: GitLab for Automated Software Delivery
        is_accordion: false
        items:
          - icon: "/nuxt-images/solutions/benefits/increase.svg"
            header: Scale your SDLC for cloud native adoption
            text: Eliminate click-ops, introduce checks and balances essential for cloud native adoption.
          - icon: "/nuxt-images/solutions/benefits/vc-and-c.svg"
            header: Every change is releasable
            text: More testing, errors detected earlier, less risk.
          - icon: "/nuxt-images/solutions/benefits/collaberation.svg"
            header: Improve employee experience
            text: Minimize repetitive tasks, focus on value generating tasks.
    - name: 'by-solution-showcase'
      data:
        title: How to automate your software delivery process
        description: To deliver higher quality applications, developers need a tool that doesn't require constant maintenance. They need a tool they can trust.
        image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
        items:
          - title: Continuous change management
            text: Coordinate, share and collaborate across your software delivery team to delivery business value faster
            list:
              - Enterprise-ready Source Code Management
              - "Track every change - code for application, infrastructure, policies, configurations"
              - Control every change - code owners, approvers, rules
              - Distributed version control for geographically distributed teams
            link:
              text: Learn more
              href: "/features/source-code-management/"
              data_ga_name:
              data_ga_location: body
            icon:
              url: /nuxt-images/solutions/showcase/gitlab-cd.svg
              alt: GitLab CD
            video: https://www.youtube.com/embed/JAgIEdYhj00?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Continuous integration and verification
            text: Accelerate your digital transformation by building high-quality applications, at scale.
            list:
              - Code, build, test automation to incrementally build & test every change
              - Less risk by detecting errors early
              - Scale with parallel builds, merge trains
              - Collaborate across projects with multi project pipeline
            link:
              text: Learn more
              href: "/stages-devops-lifecycle/continuous-integration/"
              data_ga_name:
              data_ga_location: body
            icon:
              url: /nuxt-images/solutions/showcase/continuous-integration.svg
              alt: Continous integration
            video: https://www.youtube.com/embed/ljth1Q5oJoo?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: On-demand environments with GitOps
            text: Create repeatable and stable environments by minimize risk of manual infrastructure configurations and click-ops.
            list:
              - Automate infrastructure to release faster
              - Recover from errors faster
              - Choice of push or pull configurations
              - Secure Kubernetes cluster access to avoid exposing your cluster
            link:
              text: Learn more
              href: "/solutions/gitops/"
              data_ga_name:
              data_ga_location: body
            icon:
              url: /nuxt-images/solutions/showcase/cog-code.svg
              alt: Cog Code
            video: https://www.youtube.com/embed/onFpj_wvbLM?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Continuous delivery
            text: Automate your application release process to make software delivery repeatable and on-demand.
            list:
              - Make every change ‘releasable’
              - Progressively deploy changes to minimize disruption
              - Get feedback faster by testing changes on a subset of users
            link:
              text: Learn more
              href: "/stages-devops-lifecycle/continuous-delivery/"
              data_ga_name:
              data_ga_location: body
            icon:
              url: /nuxt-images/solutions/showcase/continuous-delivery.svg
              alt: Gitlab Continuous delivery
            video: https://www.youtube.com/embed/L0OFbZXs99U?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: 'solutions-resource-cards'
      data:
        cards:
          - icon: "/nuxt-images/features/resources/icon_webcast.svg"
            event_type: "Webcast"
            header: "Collaboration without Boundaries - Faster Delivery with GitLab"
            link_text: "Read more"
            image: "/nuxt-images/features/resources/resources_webcast.png"
            href: "/webcast/collaboration-without-boundaries/"
            data_ga_name: "Collaboration without Boundaries"
            data_ga_location: "body"
          - icon: "/nuxt-images/features/resources/icon_case_study.svg"
            event_type: "Case Study"
            header: "GitLab advances open science education at Te Herenga Waka – Victoria University of Wellington"
            link_text: "Read more"
            href: "customers/victoria_university/"
            image: "/nuxt-images/features/resources/resources_case_study.png"
            data_ga_name: "GitLab advances open science education at Te Herenga Waka"
            data_ga_location: "body"
          - icon: "/nuxt-images/features/resources/icon_partners.svg"
            event_type: "Partners"
            header: "Discover the benefits of GitLab on AWS"
            link_text: "Watch now"
            href: "/partners/technology-partners/aws/"
            image: "/nuxt-images/features/resources/resources_partners.png"
            data_ga_name: "Discover the benefits of GitLab on AWS"
            data_ga_location: "body"
