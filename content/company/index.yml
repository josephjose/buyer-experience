---
  title: About GitLab | GitLab
  description: Learn more about GitLab and what makes us tick.
  components:
    - name: call-to-action
      data:
        title: About GitLab
        subtitle: Learn more about us, where we came from, and where we're headed
        centered_by_default: true
    - name: 'copy-card'
      data:
        block:
          - header: About Us
            text: |

                GitLab is The DevOps platform that empowers organizations to maximize the overall return on software development by delivering software faster and efficiently, while strengthening security and compliance. With GitLab, every team in your organization can collaboratively plan, build, secure, and deploy software to drive business outcomes faster with complete transparency, consistency and traceability.
                \
                GitLab is an open core company which develops software for the software development lifecycle with 30 million estimated registered users and more than 1 million active license users, and has an active community of more than 2,500 contributors. GitLab openly shares more information than most companies and is public by default, meaning our projects, [strategy](/company/strategy/){data-ga-name="strategy" data-ga-location="body"}, [direction](/direction/#vision){data-ga-name="direction" data-ga-location="body"} and [metrics](/company/okrs/){data-ga-name="metrics" data-ga-location="body"} are discussed openly and can be found within our website. Our values are Collaboration, Results, Efficiency, Diversity, Inclusion & Belonging , Iteration, and Transparency [(CREDIT)](/handbook/values/#credit){data-ga-name="credit" data-ga-location="body"} and these form our culture.
                \
                [GitLab's team handbook](/handbook/){data-ga-name="handbook" data-ga-location="body"}, if printed would be over 8,000 pages of text, is the central repository for how we operate and is a foundational piece to the GitLab [values](/handbook/values/){data-ga-name="values" data-ga-location="body"}.
                \
                It is GitLab's [mission](/company/mission/#mission){data-ga-name="mission" data-ga-location="body"} to make it so that everyone can contribute.
                \
                When everyone can contribute, users become contributors and we greatly increase the rate of innovation.
    - name: company-cards
      data:
        video_url: https://www.youtube.com/embed/MqL6BMOySIQ?enablesjsapi=1
        open_source_project:
          data_cards_box:
            - context: 'Used by more than'
              copy: "100,000"
              cta: '[organizations](/customers/){data-ga-name="customers" data-ga-location="body"}'
              is_large: true
            - context: 'A community of'
              copy: "2015"
              cta: |
                [code contributors](http://contributors.gitlab.com){data-ga-name="code contributors" data-ga-location="body"}
              is_large: true
          copy_media_large:
            - copy: 'We [release every month on the 22nd](/releases/categories/releases/){data-ga-name="releases" data-ga-location="body"} and there is a [publicly viewable direction](/direction/){data-ga-name="publicly viewable direction" data-ga-location="body"} for the product.'
              cta: 'Learn more from our blog'
              cta_link: /blog/
              cta_data_ga_name: blog
              cta_data_ga_location: body
              image_url: /nuxt-images/company/company-cal.svg
              image_alt_text: GitLab company calendar svg
        company_section:
          copy_media_large_one:
            - copy: 'GitLab Inc. is an open-core company that [sells subscriptions](/pricing/){data-ga-name="pricing" data-ga-location="body"} that offer more features and support for GitLab.'
              cta: 'Learn about open core'
              cta_link: /blog/2016/07/20/gitlab-is-open-core-github-is-closed-source/
              cta_data_ga_name: open source
              cta_data_ga_location: body
              image_url: /nuxt-images/company/company-tanuki.svg
              image_alt_text: GitLab company tanuki svg
            - copy: 'GitLab, the open [DevOps platform,](/solutions/devops-platform/){data-ga-name="devops platform" data-ga-location="body"} is a single application for unparalleled collaboration, visibility, and development velocity.'
              cta: 'Learn more about our product'
              cta_link: /stages-devops-lifecycle/
              cta_data_ga_name: lifecycle
              cta_data_ga_location: body
              image_url: /nuxt-images/company/company-devops.svg
              image_alt_text: GitLab company devops svg
          data_cards_box_one:
            - context: '[All remote](/company/culture/all-remote/){data-ga-name="all remote" data-ga-location="body"} with'
              copy: '1588'
              cta:  '[team members](/company/team/){data-ga-name="team members" data-ga-location="body"}'
            - context: 'Over'
              copy: '30 million'
              cta:  '[estimated registered users](/is-it-any-good/#gitlab-is-the-most-used-devops-tool){data-ga-name="most used devops tool" data-ga-location="body"}'
            - context: 'Located in'
              copy: '66'
              cta:  'countries'
          copy_media_large_two:
            - copy: 'GitLab Inc. is an active participant in this community, see our stewardship for more information.'
              cta: 'See our stewardship'
              cta_link: /company/stewardship/
              cta_data_ga_name: stewardship
              cta_data_ga_location: body
              image_url: /nuxt-images/company/company-community.svg
              image_alt_text: GitLab company community svg
              context: ''
          data_cards_box_two:
            - copy: "2011"
              cta: '[GitLab, the open source project began.](/company/history/){data-ga-name="history" data-ga-location="body"}'
              context: ''
              is_large: true
            - copy: "2015"
              cta: |
                We joined Y Combinator and started growing faster.
                Join [our team.](/jobs/){data-ga-name="jobs" data-ga-location="body"}
              context: ''
              is_large: true
          copy_media_cards_box:
            - name: 'first_block'
              data:
                - copy: 'Most of our internal procedures can be found in a [publicly viewable 5000+ page handbook](/handbook/){data-ga-name="handbook" data-ga-location="body"} and our objectives are documented in [our OKRs.](/company/okrs/){data-ga-name="OKRs" data-ga-location="body"}'
                  image_url: /nuxt-images/company/company-handbook.svg
                  image_alt_text: Gitlab company handbook icon svg
                - copy: '[Our mission](/company/mission/){data-ga-name="mission" data-ga-location="body"} is to change all creative work from read-only to read-write so that **everyone can contribute.** This is part of our overall [strategy.](/company/strategy/){data-ga-name="overall strategy" data-ga-location="body"}'
                  image_url: /nuxt-images/company/company-contribute.svg
                  image_alt_text: Gitlab company contribute icon svg
            - name: 'second_block'
              data:
                - copy: '[Our values are](/handbook/values/){data-ga-name="Our values are" data-ga-location="body"} Collaboration, Results, Efficiency, Diversity, Inclusion & Belonging , Iteration, and Transparency (CREDIT) and these form an important part of [our culture.](/company/culture/){data-ga-name="culture" data-ga-location="body"}'
                  image_url: /nuxt-images/company/company-culture.svg
                  image_alt_text: Gitlab company culture icon svg
                - copy: 'Our Tanuki (Japanese for raccoon dog) logo symbolizes our values with a smart animal that works in a group to achieve a common goal, you can download it on [our press page.](/press/){data-ga-name="press" data-ga-location="body"}'
                  image_url: /nuxt-images/company/company-tanuki.svg
                  image_alt_text: Gitlab company tanuki icon svg
