/* eslint-disable */
window.onload = (_event) => {
  const cookieBannerExists = document.getElementById('onetrust-banner-sdk');
  if (cookieBannerExists && cookieBannerExists.style.display !== 'none') {
    drift.on('ready', function (_api, _payload) {
      drift.config({
        verticalOffset: 130,
      });
    });
  }
};
