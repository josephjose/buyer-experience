import Vue from 'vue';
import * as ld from 'launchdarkly-js-client-sdk';

const clientSideIDs: { [key: string]: string } = {
  dev: '620c3008ff3d1512c4f496a3',
  test: '620c2f247f24eb0c056e3a8b',
  production: '620c2f247f24eb0c056e3a8c',
};
const user: ld.LDUser = { key: 'aa0ceb' };
const client: ld.LDClient = ld.initialize(clientSideIDs['test'], user); // LDClient must remain a singleton
// Docs on Matter: https://docs.launchdarkly.com/sdk/client-side/javascript#initializing-the-client

declare module 'vue/types/vue' {
  // Taken from https://typescript.nuxtjs.org/cookbook/plugins#plugin
  interface Vue {
    $launchdarkly(flag: string): boolean;
  }
}
// Promise is evaluated when launch-darkly.vue is rendered onto a page
Vue.prototype.$launchdarkly = (flag: string) =>
  client
    .waitForInitialization()
    .then(() => {
      // initialization succeeded, flag values are now available
      return client.variation(flag, false) as boolean;
    })
    .catch(() => false); // Show control if promise fails
