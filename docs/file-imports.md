# File Imports

The project architecture uses `Barrel Imports` for each module/folder 
inside the `components` directory in order to have a more consistent, organized and modularized project.

## Considerations to have

1. When a new folder is created inside the `components` directory, it should have an `index.vue file
that will have all the files from the folder exported from there.

- Example: `components/features/index.vue`

```
    <script lang="ts">
    export const Features = {
      name: 'FeaturesIndex',
    };
    
    export { default as FeatureBenefit } from './feature-benefit.vue';
    export { default as FeatureBlockHero } from './feature-block-hero.vue';
    export { default as FeatureBlockSummary } from './feature-block-summary.vue';
    export { default as FeatureGetStarted } from './feature-get-started.vue';
    export { default as FeatureOverview } from './feature-overview.vue';
    export { default as FeatureValueProposition } from './feature-value-proposition.vue';
    export { default as FeaturesBlockRelated } from './features-block-related.vue';
    export { default as FeaturesCategories } from './features-categories.vue';
    export { default as FeaturesContent } from './features-content.vue';
    export { default as FeaturesCopyMedia } from './features-copy-media.vue';
    export { default as FeaturesFilter } from './features-filter.vue';
    export { default as FeaturesLandingHero } from './features-landing-hero.vue';
    export { default as FeaturesMenu } from './features-menu.vue';
    export { default as FeaturesProductsCategoryCards } from './features-product-category-cards.vue';
    
    export default Features;
    </script>
 ```


2. After the `index.vue` file is created, the Barrel imports can be used like the example below:

```
import { FeatureOverview, FeatureBenefit, FeatureBlockSummary, FeaturesBlockRelated as FeatureBlockRelated } from '../../components/features/index';
```


3. Components should not import other components from the same module using the `index.vue` file,
that will cause a `Circular Dependency` error, in those cases the imports needs to be directly to the file.


4. **Important Note**: Circular dependency errors are not well specified by Vue. When a circular dependency error occurs due to importing
files from the same module, the vue error will throw a message like `Unknown custom element: - did you register the component correctly? For recursive components, make sure to provide the "name" option.`

