# Plugins

## Render markdown with `markdown-it`

[This plugin](https://github.com/markdown-it/markdown-it) is a markdown parser that enables markdown syntax, defined in our content files, to be rendered in the page with the help of `$md.render()` method.

In order to use it:

1. Create a markdown attribute inside the `content` folder as shown here:

2. Render the content attrbitue created in step 1 inside the `.vue` component in which you want to see the markdown, as shown below:

### Anchor links with `markdown-it-anchor`

[This plugin](https://www.npmjs.com/package/markdown-it-anchor) will generate an anchor link every time a heading is defined in the markdown attribute defined above.

To use it, define a new heading in the markdown attribute you have setup for your content and is rendered by the `$md.render()` method.

| Markdown  |  Rendered Output |
|---|---|
| `# Heading level 1` |`<h1 id="heading-level-1">Heading level 1 <a class="header-anchor" href="#heading-level-1">...</a></h1>`  |  
| `## Heading level 2`|`<h2 id="heading-level-2" tabindex="-2">Heading level 2 <a class="header-anchor" href="#heading-level-2">...</a></h2>`|  
| `### Heading level 3`|`<h3 id="heading-level-3">Heading level 3 <a class="header-anchor" href="#heading-level-3">...</a></h3>`  |  
| `#### Heading level 4` |`<h4 id="heading-level-4">Heading level 4 <a class="header-anchor" href="#heading-level-4">...</a></h4>`  |
| `##### Heading level 5` |`<h5 id="heading-level-5">Heading level 5 <a class="header-anchor" href="#heading-level-5">...</a></h5>`  |
| `###### Heading level 6` |`<h6 id="heading-level-6">Heading level 6 <a class="header-anchor" href="#heading-level-6">...</a></h6>`  |

#### Styles

To change styles or the hover behavior, please go to the `base.scss` scss assets file and change the `.header-anchor` selector.

#### Additional `markdown-it-anchor` configuration

To change the icon, position relative to the text and/or additional settings, please go to `markdown-it-anchor` attribute defined in the `markdownit` object, both defined inside the `nuxt.config.js` file.

---

## Render diagrams with `Mermaid.js and Vue-Mermaid-String`
Mermaid.js will turn Markdown or code into diagrams and visualizations. Further documentation can be found [here](https://mermaid-js.github.io/mermaid/#/).

Mermaid is currently implemented in this project through the use of the `vue-mermaid-string` npm [package](https://www.npmjs.com/package/vue-mermaid-string) and component. This wrapper allows data strings to be converted into Mermaid diagrams, which means the data entered in Yaml/content files will be usable.

### Import the library into your page or component
To add Vue-Mermaid-String functionality, you can import the library between your Vue file's script tags.

```
import VueMermaidString from 'vue-mermaid-string';
```

### The `mermaid-chart.vue` component

You can add a Mermaid chart to your page by importing `mermaid-chart.vue` to your page. In your yaml file, you will want to use this schema:

```
    - name: mermaid-chart
      data:
        chart_info: |
          gantt
              title Year Overview
              dateFormat  YYYY-MM-DD

              section Fiscal Year at a Glance
              FY starts                   :2021-02-01, 1d
              End of Q1                   :2021-04-30, 1d
              End of Q2                   :2021-07-31, 1d
              End of Q3                   :2021-10-31, 1d
```

You can replace this gantt chart example with the diagram type and data that you need.
---
## Running AB tests with `launch-darkly.vue`

Go to the page that you want to run an experiment on
```javascript
// ~/pages/enterprise.vue
import LaunchDarkly from '~/components/launch-darkly.vue'
```

Add to the `components` object in the component declaration
```javascript
  components: {
    YourOtherComponent,
    LaunchDarkly
  },
```

Go into the Launchdarkly interface, navigate to the Buyer's Experience project, then create a feature flag here.
TODO: Further define this project here. 

When you've done that, add this to the `<template>`
```vue
<LaunchDarkly featureFlag="your-feature-flag"> 
  <template #experiment>
  // Your experiment goes here
  </template>
  <template #control>
  // Your control goes here
  </template>
</LaunchDarkly>
```

You can force a page to show either the control/experiment by passing `?experiment-review-control` or `?experiment-review-test` respectively as a URL parameter. 

If you do not author the `control` and `experiments` slots, a warning will be thrown out on the appropriate page. 
 