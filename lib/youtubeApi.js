/* eslint-disable */
export function initializeYouTubeIframeApi() {
  var initialized = false;
  var allScripts = document.getElementsByTagName('script');
  for (var i = 0; i < allScripts.length; i++) {
    if (allScripts[i].src === 'https://www.youtube.com/iframe_api') {
      initialized = true;
    }
  }
  if (initialized) {
    return;
  }
  var tag = document.createElement('script');
  tag.src = 'https://www.youtube.com/iframe_api';
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

export function createYouTubePlayer(identifier, onReady) {
  window[`playerFor${identifier}`] = undefined;
  window.onYouTubeIframeAPIReady = function () {
    window[`playerFor${identifier}`] = new window.YT.Player(
      `iframe-for-${identifier}`,
      {
        events: {
          onReady,
        },
      },
    );
  };
}
